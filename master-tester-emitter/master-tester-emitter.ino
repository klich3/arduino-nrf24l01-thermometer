/*
  sensor - lux

  Author: Anthony Sychev

  transmitter nRF20L01+

  Tester

  edit type value for test emmitter or receiver

*/

// -- Includes --

// Radio
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

// -- Global variables --
int type = 2; //1 emitter - sensor | 2 receiver - master

// Single radio pipe address for the 2 nodes to communicate.
const uint64_t pipe = 0xE8E8F0F0E1LL;
int sensorValue = 0;

RF24 radio(9, 10);

void setup()
{
  delay(20);
  Serial.begin (9600);
  printf_begin();

  // Initiate the radio
  radio.begin();

  // Max power
  radio.setPALevel( RF24_PA_MAX );

  // Min speed (for better range I presume)
  radio.setDataRate( RF24_250KBPS );

  // 8 bits CRC
  radio.setCRCLength( RF24_CRC_16 );

  radio.enableDynamicPayloads();
  radio.setRetries(15, 15);
  radio.setChannel(108);

  if (type == 1)
  {
    // Open a writing pipe on the radio
    radio.openWritingPipe(pipe);
    radio.printDetails();
    Serial.println("Init lux tester emitter - sensor");
  } else {
    radio.openReadingPipe(1, pipe);
    radio.startListening();

    radio.printDetails();
    radio.testCarrier();
    radio.testRPD();

    Serial.println("Init lux tester receiver - master");
  }

}

void loop()
{
  if (type == 1)
  {
    //sensor

    Serial.println("Send value");
    // Write the temperature to the pipe
    sensorValue = 500;
    radio.write(&sensorValue, sizeof(int));
  } else {
    //master

    if (radio.available()) radio.read(&sensorValue, sizeof(int));
    Serial.print("Recibe value ");
    Serial.println(sensorValue);
  }
  delay(800);
}
