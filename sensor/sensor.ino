/*
  sensor - lux

  Author: Anthony Sychev

  transmitter nRF20L01+

  every 10min send luminous sensor value using Flying-Fish
  with fours pins (vcc, gdn, A0 and tx/D0).

  sensor values: 0 - 120 is a day on 12 - 16h
                450 - 535 is a night like 21h

  This script is for first arduino nano only for send sensor value.

  For upload firmware to arduino you must disconect RX/Tx (rst) pin from sensor.

  Sensor pins connections:
  
  Analog pin (A0)
  Digital pin (rst) not used

  info: http://www.theengineeringprojects.com/wp-content/uploads/2015/10/NRF24L01-with-Arduino-Response-Timed-Out2.jpg
  info sensor: https://arduino-info.wikispaces.com/Brick-LightSensor-Analog-Digital
*/

// -- Includes --

// Sleep
#include <avr/sleep.h>
#include <avr/power.h>

// Watchdog timer
#include <avr/wdt.h>

// Radio
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

// -- Global variables --
bool debug = true;

int counter = 0;
volatile int f_wdt = 1;

//sensor pin
int sensorPin = A0;

//sensor value
int sensorValue = 0;

// Single radio pipe address for the 2 nodes to communicate.
const uint64_t pipe = 0xE8E8F0F0E1LL;

RF24 radio(9, 10);

// -- Functions --
void counterHandler()
{
  // Increment the sleep counter
  counter++;

  // Should be 75 for 10 minutes (75 * 8 = 600 seconds = 10 minutes)
  // Use 1 for debugging purposes
  int minsCounter = 0;
  if (debug)
  {
    minsCounter = 1;
  } else {
    minsCounter = 75;
  }

  if (counter == minsCounter) {
    // Reset the counter to 0
    counter = 0;

    // Power up components
    power_all_enable();

    // Power up the radio
    radio.powerUp();

    // Wait for radio to power up
    delay(2);
  } else {
    // Sleep time isn't over yet, sleep some more
    enterSleep();
  }
}

void enterSleep()
{
  // Start the watchdog timer
  f_wdt = 0;

  // Power down the radio
  radio.powerDown();

  // Enter sleep
  sleep_enable();
  sleep_mode();

  // Wake from sleep
  sleep_disable();

  // Increment the interrupt counter
  counterHandler();
}

ISR(WDT_vect)
{
  // Stop the watchdog timer
  f_wdt = 1;
}

void setupWDT()
{
  // Setup the Watchdog timer for an interruption every 8 seconds
  MCUSR &= ~(1 << WDRF);
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  WDTCSR = 1 << WDP0 | 1 << WDP3;
  WDTCSR |= _BV(WDIE);
}

void setup()
{
  delay(20);
  if (debug) Serial.begin (9600);
  printf_begin();

  // Disable Brown out detection (uses power)
  sleep_bod_disable();

  // Sleep mode setup
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);

  // Watchdog timer setup
  setupWDT();

  //radio setup

  // Initiate the radio
  radio.begin();

  // Max power
  radio.setPALevel( RF24_PA_MAX );

  // Min speed (for better range I presume)
  radio.setDataRate( RF24_250KBPS );

  // 8 bits CRC
  radio.setCRCLength( RF24_CRC_16 );

  radio.enableDynamicPayloads();
  radio.setRetries(15, 15);
  radio.setChannel(108);

  // Open a writing pipe on the radio
  radio.openWritingPipe(pipe);
  radio.printDetails();
}

void loop()
{
  sensorValue = analogRead(sensorPin);

  if (debug) {
    Serial.print("Sending sensor value: ");
    Serial.println(sensorValue);
  }

  // Write the temperature to the pipe
  //bool ok = radio.write(&sensorValue, sizeof(int));
  radio.write(&sensorValue, sizeof(int));

  // Sleep time
  enterSleep();
}
