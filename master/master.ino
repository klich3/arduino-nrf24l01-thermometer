/*
  sensor - lux

  Author: Anthony Sychev

  transmitter nRF20L01+
  RTC (tyny RTC)

  every 10min send luminous sensor value using Easy Fish
  with fours pins (vcc, gdn, A0 and tx/D0).

  sensor values: 0 - 120 is a day on 12 - 16h
                450 - 535 is a night like 21h

  This script is for second arduino nano hire calc time
  and control relay for turn on/off outside lights.

  1) sensor -> send val -> master
  2) master check val < 320 = day off lights
                      > 320 (450 - 535) = night on light +
     time counter 4h. After that wait or ignore sensor val
     6h.

    info: http://www.theengineeringprojects.com/wp-content/uploads/2015/10/NRF24L01-with-Arduino-Response-Timed-Out2.jpg

*/

// -- Includes --
#include <Wire.h>     //RTC
#include <RTClib.h>   //RTC
#include <nRF24L01.h> // wifi module
#include <RF24.h>     // wifi module

// -- Conection --
#define relayPin 5    //PIN to activate/desacivate relay
#define rtcOnePin A4  //RTC SDO a4 sda
#define rtcTwoPin A5  //RTC SDA a5 scl

RTC_DS1307 RTC;

RF24 radio(9, 10); //init wifi module (9, 10) digital pin
const uint64_t pipe = 0xE8E8F0F0E1LL; //pipe comunication with sensor wifi

/*
  sensor value:
  450 - 19-20h August
  580 - 18h November
*/
int sensorValueEquivalNight = 540; // consider night
int sensorValue = 0; //default sensor value
int fn_state = 0;
int onLightHour = 19;
int offLightHour = 2;

// -- Functions --
void setup()
{
  Serial.begin(115200);

  //RTC
  Wire.begin(); //configura el bus I2C estableciendo arduino como MASTER
  RTC.begin();

  if (! RTC.isrunning()) // se verifica si el modulo rtc esta en funcionamiento, de lo contrario, se imprime: RTC is NOT running!
  {
    Serial.println("> RTC esta desactivado!");
    delay(10000);
  }

  //RTC.adjust(DateTime(__DATE__, __TIME__)); //esta funcion establecera en el modulo la fecha de creación del archivo .hex generado al compilar el sketch.
  //Serial.println("> RTC Syncronizando el reloj");
  delay(10000);

  //relay
  pinMode(relayPin, OUTPUT); //relay setup
  digitalWrite(relayPin, HIGH); //light off by default

  //radio setup
  radio.begin();

  // Max power
  radio.setPALevel(RF24_PA_MAX);

  // Min speed (for better range I presume)
  radio.setDataRate(RF24_250KBPS);

  // 8 bits CRC
  radio.setCRCLength(RF24_CRC_16);

  radio.enableDynamicPayloads();
  radio.setRetries(15, 15);
  radio.setChannel(108);

  radio.openReadingPipe(1, pipe);
  radio.startListening();

  Serial.println("> SETUP");
}

void loop()
{
  DateTime now = RTC.now(); //obtiene datos del modulo RTC

  if (radio.available()) radio.read(&sensorValue, sizeof(int));
  Serial.print("> RADIO sensor [" + String(sensorValue));
  Serial.println("]");

  Serial.println("> INIT tiempo [" + String(now.hour()) + ":" + String(now.minute()) + ":" + String(now.second()) + "] esperando las 20h tic tac");
  delay(1000);

  onOffByCalendar(now); //ajustamos el encendido de la luzes segun el calendario

  switch (fn_state)
  {
    case 0:
      if (now.hour() > onLightHour) fn_state = 1; //miramos radio
      break;

    case 1:
      if (sensorValue != 0)
      {
        if (now.minute() <= 3) fn_state = (&sensorValue < sensorValueEquivalNight) ? 2 : 3;
      } else {
        if (now.hour() > onLightHour)  fn_state = 2;
      }
      break;

    case 2:
      Serial.println("> NIGHTON estamos de noche y encendemos por 6h");
      if (digitalRead(relayPin) == HIGH) digitalWrite(relayPin, LOW); //on
      sensorValue = 0;
      fn_state = 4;
      break;

    case 3:
      /*
        apagamos las luces y volvemos a esperar las 20h
      */
      Serial.println("> DAYOFF estamos de dia apagamos todo");
      if (digitalRead(relayPin) == LOW) digitalWrite(relayPin, HIGH); //off
      sensorValue = 0;
      fn_state = 0;
      break;

    case 4:
      Serial.println("> NIGHTCOUNT esperamos a las 02:00 para apagar las luces.");

      if (now.hour() >= 20) return; //formato es de 24h cuando supera 23h empieza actuar el apagado.

      if (now.hour() >= offLightHour)
      {
        if (digitalRead(relayPin) == LOW) digitalWrite(relayPin, HIGH); //off
        fn_state = 3; //apagamos la luz initiamos espera hasta las 20h
      }
      break;
  }
}

void onOffByCalendar(DateTime now)
{
  int month = now.month();

  Serial.println("Month >");
  Serial.println(month);

  switch (month)
  {
    case 1:
    case 2:
    case 12:
      onLightHour = 18;
      offLightHour = 1;
      break;

    case 3:
    case 11:
      onLightHour = 19;
      offLightHour = 1;
      break;

    case 4:
    case 10:
      onLightHour = 19; //tiempo verano
      offLightHour = 1;
      break;

    case 5:
    case 6:
    case 8:
    case 9:
      onLightHour = 20;
      offLightHour = 1;
      break;

    case 7:
      onLightHour = 22;
      offLightHour = 2;
      break;


    default:
      onLightHour = 19;
      offLightHour = 2;
      break;
  }
}

